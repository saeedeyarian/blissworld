import React from 'react';

//css
import styles from './scss/SlideShow.module.scss';
//image
import VitaminC from '../assets/carousel-vitamin-c-tinycompress-min.jpg';

const SlideShow = () => {
    return (
        <div className={styles.container}>
            <img src={VitaminC} alt='VitaminC'/>
            <div className={styles.backgroundColor}>
            </div>
                <div className={styles.textContainer}>
                    <h1><a href='google.com'>Bright Idea</a></h1>
                    <p>Powerd by clinic grade vitamin C, this bestselling <br/>
                        trio boosts brightness & reduces the look of dark sports and for visibily glowing results
                    </p>
                    <button className={styles.shopBrightButton}>SHOP BRIGHT IDEA</button>
                </div>
        </div>
    );
};

export default SlideShow;