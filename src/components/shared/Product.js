import React from 'react';

//css
import styles from '../scss/Product.module.css';

const Product = ({productData}) => {
    return (
        <div className={styles.container}>
            <img className={styles.productImage} src={productData.image} alt="product" />
            <button className={styles.detailsButton}>QUICK VIEW</button>
            <p className={styles.productName}>{productData.name}</p>
            <p className={styles.title}>skin enhancing glowy serum</p>
            <button className={styles.AddBage}>Add to bag $25</button>
        </div>
    );
};

export default Product;