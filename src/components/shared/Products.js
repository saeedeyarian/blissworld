import React, { useContext } from 'react';
import { Link } from 'react-router-dom' ;
//context
import { cartContext } from '../../context/CartContextProvider';
//func
import {isInBag, quantityCounter, shorten} from '../functions/shorten';
//css
import styles from '../scss/Products.module.css';

const Products = ({productData}) => {

    const {state,dispatch} = useContext(cartContext)

    return (
        <div className={styles.container}>
            <div className={styles.productImages}>
                <img src={productData.image} alt='product'/>
                <Link to={`/${productData.id}`}><button className={styles.detailsButton}>QUICK VIEW</button></Link>
            </div>
           <p className={styles.category}>{productData.category}</p>
           <p className={styles.productTitle}>{shorten(productData.title)}</p>
           <div className={styles.buttonContainer}>
                { isInBag (state, productData.id) ?
                    <button className={styles.smallButton} onClick={() => dispatch ({type:"INCREASE", payload: productData})}>+</button> :
                    <button onClick={() => dispatch ({type:"ADD_ITEM", payload: productData})}>ADD TO BAG {productData.price}$</button>
                }
                {quantityCounter(state,productData.id) >0 && <span>{quantityCounter(state,productData.id)}</span>}
                { quantityCounter(state,productData.id) > 1 && <button className={styles.smallButton} onClick={() => dispatch ({type:"DECREASE", payload: productData})}> - </button>}
                { quantityCounter(state,productData.id) === 1 && <button className={styles.smallButton} onClick={() => dispatch ({type:"REMOVE_ITEM", payload: productData})}> - </button>}
           </div>
        </div>
    );
};

export default Products;