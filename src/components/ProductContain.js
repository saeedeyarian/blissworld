import React, { useContext } from 'react';

//css
import styles from './scss/ProductContain.module.css';
//components
import Product from './shared/Product';
//img
import loading from '../assets/loading.svg'
//context
import { productContext } from '../context/ProductContextProvider';

const ProductContain = () => {

    const products = useContext(productContext)

    return (
        <div className={styles.Container}>
            <div className={styles.textContainer}>
                <h1>summer refresh</h1>
                <p>say hello to smooth,hydrated,glowing skin!</p>
            </div>
            {products.length > 0 ?
                <div className={styles.productContainer}>
                    {products.map ( product => <Product key={product.id} productData={product}/>)} 
                </div> :
                    <img src={loading} alt="loading"/>
                }
        </div>
    );
};

export default ProductContain;