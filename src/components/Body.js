import React from 'react';

//components
import SlideShow from './SlideShow';
import ProductContain from './ProductContain';
import NewProducts from './NewProducts';
import BrightIdeas from './BrightIdeas';


const Body = () => {
    return (        
        <div style={{width:"fitContent"}}>
            <SlideShow/>
            <ProductContain/>
            <NewProducts/>
            <BrightIdeas/>
        </div>
    );
};

export default Body;