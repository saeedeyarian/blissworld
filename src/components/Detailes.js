import React, { useContext } from 'react';
import { Link, useParams } from 'react-router-dom';
//context
import { productContext } from '../context/SecondProductContextProvider';

//css
import styles from './scss/Detailes.module.css';

const Detailes = () => {

    const data = useContext(productContext);
    const params = useParams();
    const id = params.id;
    const product = data[id-1];

    const {image,title,category,price,description} = product

    return (
        <div className={styles.container}>
            <img src={image} alt='product'/>
            <div className={styles.textContainer}>
                <h3 className={styles.title}>{title}</h3>
                <p className={styles.description}>{description}</p>
                <p className={styles.category}><span>category:</span> {category}</p>
                <div className={styles.buttonContainer}>
                    <p className={styles.price}>{price} $</p>
                    <Link to="/">Back to store</Link>
                </div>
            </div>
        </div>
    );
};

export default Detailes;