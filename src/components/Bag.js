import React, { useContext } from 'react';

import { cartContext } from '../context/CartContextProvider';
//css
import styles from "./scss/Bag.module.css";
import { shorten } from './functions/shorten';

const Bag = (props) => {

    const {dispatch} = useContext(cartContext)
    const {image,category,price,title,quantity} = props.data;

    return (
        <div className={styles.container}>
            <img src={image} alt='product'/>
            <div className={styles.elementContainer}>
            <div className={styles.textContainer}>
                <p className={styles.categoryText}>{category}</p>
                <p className={styles.titleText}>{shorten(title)}</p>
            </div>
            <div className={styles.buttonContainer}>
                {quantity > 1 ?
                    <button  onClick={() => dispatch ({type:"DECREASE", payload:props.data})}>-</button> :
                    <button>-</button>
                }
                <span>{quantity}</span>
                <button onClick={() => dispatch ({type:"INCREASE", payload:props.data})}>+</button>
            </div>
            <p className={styles.price}>{price*quantity} $</p>
            </div>
            {<span className={styles.removeButton} onClick={() => dispatch ({type:"REMOVE_ITEM", payload:props.data})}>Remove</span>}
        </div>
    );
};

export default Bag;