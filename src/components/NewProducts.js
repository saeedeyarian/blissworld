import React from 'react';

//img
import surprise from '../assets/surprises.gif';
import truck from '../assets/truck.gif';
import samples from '../assets/samples.gif';
import assured from '../assets/rest-assured.webp';
import febulips from '../assets/fabulips-retina.jpg';

//css
import styles from './scss/NewProducts.module.css';


const NewProducts = () => {
    return (
        <div className={styles.container}>
            <section className={styles.only_bliss_section}>
                <div className={styles.onlyBliss_container}>
                <div className={styles.textContainer}>Only on blissworld.com</div>
                    <ul >
                        <li>
                            <div><img src={truck} alt='truck'/></div>
                            <div>
                                <p>FREE SHOPING</p>
                                <p>(on order over $40)</p>
                            </div>
                        </li>
                        <li>
                            <div><img src={samples} alt='samples'/></div>
                            <div>
                                <p>FREE SAMPLES</p>
                                <p>(YES,please!)</p>
                            </div>
                        </li>
                        <li>
                            <div><img src={surprise} alt='surpriseBox'/></div>
                            <div>
                                <p>SURPRISES</p>
                                <p>(You will love,Trust.)</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
            <section className={styles.second_section}>
                <div className={styles.second_sectionContainer}>
                    <div className={styles.elementContain}>
                    <img className={styles.productImage} src={febulips} alt='febulips'/>
                        <div className={styles.text}>
                            <p>NEW! Fabulips™</p>
                            <p>Three new ways to pamper & nourish your pout</p>
                            <button>SHOP FABULIPS</button>
                        </div>
                    </div>
                    <div className={styles.elementContain}>
                        <img className={styles.productImage}  src={assured} alt='assured'/>
                        <div className={styles.text}>
                            <p>NEW! Rest Assured™</p>
                            <p>Visibly reduces dark circles & depuffs for refreshed eyes</p>
                            <button>SHOP REST ASSURED</button>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default NewProducts;