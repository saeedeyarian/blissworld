import React, { useContext } from 'react';
//css
import styles from './scss/ShoppingBag.module.css';
//component
import Bag from './Bag';
//context
import { cartContext } from '../context/CartContextProvider';
//image
import paypal from '../assets/paypal.svg'

const ShoppingBag = () => {

    const {state,dispatch} = useContext(cartContext);

    return (
        <>
            <div className={styles.hedaer}>
               <h1>Shopping Bag</h1> 
            </div>
            {state.itemCounter > 0 &&
                <div className={styles.container}>
                    <div className={styles.productContainer}>
                    {state.selectedItem.map ( item => <Bag key={item.id} data={item}/>)}
                    </div>
                    <div className={styles.paymentBox}>
                        <div className={styles.paymentContainer}>
                            <div className={styles.paymentTextContainer}>
                                <div>
                                    <span>Subtotal:</span>
                                    <span>${state.total.toFixed(2)}</span>
                                </div>
                                <div>
                                    <span>Shopping:</span>
                                    <span>Add Info</span>
                                </div>
                                <div>
                                    <span>Coupon Code:</span>
                                    <span>Add Coupon</span>
                                </div>
                                <div>
                                    <span>Grand total:</span>
                                    <span>${state.total.toFixed(2)}</span>
                                </div>
                                <p>or 4 interest-free payments of $2.00 with</p>
                                <div className={styles.afterpayContainer}>
                                <button className={styles.afterpayButton}>afterpay</button>
                                </div>
                            </div>
                        </div>
                        <div style={{direction:"rtl"}}>
                            <button className={styles.checkoutBox} onClick={() => dispatch ({type:"CHECKOUT"})}>CHECKOUT</button>
                        </div>
                        <div className={styles.paypalContainer}>
                            <p>--or use--</p>
                            <button><img src={paypal} alt='paypalLogo'/></button>
                        </div>
                    </div>
                </div>
            }
                {
                    state.checkout && <div className={styles.compeleted}><h3>Checkout Successfully</h3></div>
                }
        </>
    );
};

export default ShoppingBag;