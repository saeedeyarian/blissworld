import React, { useContext } from 'react';
//context
import { productContext } from '../context/SecondProductContextProvider';
//componnets
import Products from './shared/Products';
//img
import loading from '../assets/loading.svg';
//css
import styles from './scss/BrightIdeas.module.css';

const BrightIdeas = () => {

    const product = useContext(productContext)

    return (
        <div className={styles.container}>
            <div className={styles.brightHeader}>
                <h1>Bright Idea</h1>
                <p>Boosts Brightness & Elasticity In 1 Use With Clinical-Grade Vitamin C</p>
            </div>
            {product.length > 1 ?
            <div className={styles.productContainer}>
                {product.map( product => <Products key={product.id} productData={product}/>)}
            </div> :
            <img src={loading} alt='loading'/>
            }
        </div>
    );
};

export default BrightIdeas;