import React, { useState } from 'react';

//css
import styles from './scss/Footer.module.css';

//img
import bcorp from '../assets/bcorp.svg';
import instagram from '../assets/InstagramLogo.svg';
import youtube from '../assets/youtube-logo.svg';
import tiktok from '../assets/TikTok-Icon.svg';

const Footer = () => {

    const [showCompany, setShown] = useState(false)
    const [showCostumer, setShowCostumer] = useState(false)
    const [showdrtStuff, setShowDryStuff] = useState(false)


    const companyHandler = () => {
            setShown(!showCompany)
        };
    const costumHadler = () => {
        setShowCostumer(!showCostumer)
        };
    const dryStuff = () => {
        setShowDryStuff(!showdrtStuff)
    }

    return (
        <div style={{position:"relative"}}>
        <footer className={styles.footerContainer}>
            <section className={styles.leftSection}>
                <img className={styles.bcorpImage} src={bcorp} alt='bcorpImage'/>
                <nav className={styles.footerNavManue}>
                    <ul>
                        <p>COMPANY</p>
                        <li  className={showCompany ? styles.active : styles.inActive}>Who we are</li>
                        <li className={showCompany ? styles.active : styles.inActive}>Find Bliss Near You</li>
                        <li className={showCompany ? styles.active : styles.inActive}>Bliss Spa</li>
                        <li className={showCompany ? styles.active : styles.inActive}>Shop</li>
                        <button className={styles.plussButton} onClick={companyHandler}>+</button>
                    </ul>
                    <ul>
                        <p>CUSTOMER CARE</p>
                        <li className={showCostumer ? styles.active : styles.inActive}>Gift Cards</li>
                        <li className={showCostumer ? styles.active : styles.inActive}>Contact Us</li>
                        <li className={showCostumer ? styles.active : styles.inActive}>Shopping</li>
                        <li className={showCostumer ? styles.active : styles.inActive}>Returns</li>
                        <li className={showCostumer ? styles.active : styles.inActive}>Ordering and Payment</li>
                        <li className={showCostumer ? styles.active : styles.inActive}>SMS Texts and Alerts</li>
                        <button className={styles.plussButton} onClick={costumHadler}>+</button>
                    </ul>
                    <ul>
                        <p>The Dry Stuff</p>
                        <li className={showdrtStuff ? styles.active : styles.inActive}>Privacy Policy</li>
                        <li className={showdrtStuff ? styles.active : styles.inActive}>Terms and Conditions</li>
                        <li className={showdrtStuff ? styles.active : styles.inActive}>Accessibility Statment</li>
                        <li className={showdrtStuff ? styles.active : styles.inActive}>Sitemap</li>
                        <button className={styles.plussButton} onClick={dryStuff} >+</button>

                        <div className={styles.funStaffContainer}>
                            <p>THE FUN STUFF</p>
                            <div className={styles.socialmedia_logo}>
                                <img src={instagram} alt='instagramLogo'/>
                                <img src={youtube} alt='youtubeLogo'/>
                                <img src={tiktok} alt='tiktokLogo'/>
                            </div>
                        </div>
                    </ul>
                </nav>
            </section>
            <section className={styles.rightSection}>
                <h3>Hear the lastest</h3>
                <p> (new products, exclusive offers and other suprises) </p>
                <input type='text' placeholder='Enter you Email ...'/>
                <button className={styles.submitButton}>submit</button>
            </section>
        </footer>
        <div className={styles.socialmediaLogo}>
            <img src={instagram} alt='instagramLogo'/>
            <img src={youtube} alt='youtubeLogo'/>
            <img src={tiktok} alt='tiktokLogo'/>
        </div>
        <div className={styles.text}>
            <p>©2022 BlissWorld Inc.</p>
        </div>
        </div>
    );
};

export default Footer;