import React from 'react';
import { shorten } from './functions/shorten';
import styles from './scss/SearchingProducts.module.css';

const SearchingProducts = ({productData}) => {
    return (
        <div className={styles.container}>
            <img src={productData.image} alt='product'/>
            <div>
                <p>{productData.category}</p>
                <p>{shorten(productData.title)}</p>
            </div>
        </div>
    );
};

export default SearchingProducts;