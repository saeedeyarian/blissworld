export const Validate = (data) => {
    const errors = {};

    if (!data.email) {
     errors.email = "Please use a valid email address,such as user@example.com."   
    } else if (!/\S+@\S+\.\S+/.test(data.email)) {
        errors.email = "Please use a valid email address,such as user@example.com."
    } else {
        delete errors.email
    }
    if (!data.password) {
        errors.password = "You must enter a password."
    } else {
        delete errors.password
    }

    return errors
}