import React, { useEffect, useState } from 'react';

//tostify
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

//component
import { notify } from './tostify';
import { Validate } from './Validate';

//css
import styles from '../scss/Login.module.css';

const Login = () => {

    const [data, setData] = useState ({
        email: "",
        password: "",
    })

    const [errors, setErrors] = useState({});
    const [touched, setTouched] = useState({})

    useEffect ( () => {
        setErrors (Validate(data))
    }, [data,touched])

    const changeHandler = event => {
        setData({...data, [event.target.name] : event.target.value})
    }
    const focusHandler = event => {
        setTouched({...touched, [event.target.name] : true})
    }

    const submitHandler = event => {
        event.preventDefault();
        if (!Object.keys(errors).length) {
            notify("You loged in successfully!", 'success')
        }else {
            notify("invalid data!", 'error')
            setTouched ({
                email:true,
                password:true
            })
        }
    }

    return (
        <div className={styles.container}>
            <form className={styles.loginBox} onSubmit={submitHandler}>
                <div className={styles.loginContainer}>
                    <h3>Honey, You're Home</h3>
                    <p>Welcome back! Sign into your account here:</p>

                    <input
                        placeholder='Email Address:'
                        type='text'
                        name='email'
                        value={data.email}
                        onChange={changeHandler}
                        onFocus={focusHandler}
                        />
                    {errors.email && touched.email && <span>{errors.email}</span> }
                    <input  
                        placeholder='Password:'
                        type='password'
                        name='password'
                        value={data.password}
                        onChange={changeHandler}
                        onFocus={focusHandler}
                        />
                    {errors.password && touched.password && <span>{errors.password}</span> }

                    <button>SIGN IN</button>
                    <p>Forgot your password?</p>
                </div>
            </form>
            <div className={styles.registerBox}>
                <div className={styles.registerContainer}>
                    <h3>oh, Hey.</h3>
                    <div><p> you know? When you create a blissworld.com account, you get access to cool stuff like:</p></div>
                    <div>
                        <p>- an easy, breezy checkout process <br />
                        - quick access to orders & tracking</p>
                    </div>
                    <button>REGISTER</button>
                </div> 
            </div>
            <ToastContainer />
        </div>
    );
};

export default Login;