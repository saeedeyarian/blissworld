import React, { useState } from 'react';

//css
import styles from './scss/HammenueIcon.module.css';

function HamMenueIcon() {

    const [style, setStyle] = useState(false)
    const changeStyle = () => {
        setStyle(!style);
    }

    return (
        <div className={styles.container} onClick={changeStyle}>
            <span className={ style ? styles.changeBar1 : styles.changeBar1A}></span>
            <span className={ style ? styles.changeBar2 : styles.changeBar2B}></span>
            <span className={ style ? styles.changeBar3 : styles.changeBar3C}></span>
        </div>
    );
}

export default HamMenueIcon;