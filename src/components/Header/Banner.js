import React, { useState } from 'react';

//images
import closed from '../../assets/icon-top-banner-close-X.svg'
//css
import styles from './scss/Banner.module.css';

const Banner = () => {

    const[isActive, setIsActive] = useState(true);
    const hideBanner =() => {
        setIsActive (false);
    }

    return (
        <div>
           <div className={isActive ? styles.bannerContainer : styles.hideBanner}>
                <p className={styles.bannerText}> SHOP MORE, SAVE MORE! Take $10 Off $40 or $20 Off $65 with code YESBLISS at checkout</p>
                <img onClick={hideBanner} src={closed} alt="closedAsign"/>
            </div>
        </div>
    );
};

export default Banner;