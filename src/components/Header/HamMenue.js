import React, { useState } from 'react';

//css
import styles from './scss/HamMenue.module.css';

//img
import arrow from '../../assets/arrow.png';

const HamMenue = () => {

    const [shownOne,setShownOne] = useState(false);
    const [shownTwo,setShownTwo] = useState(false);
    const [shownThree,setShownThree] = useState(false);
    const [showSubMenueItemOne, setShowSubMenueItemOne] = useState(false);
    const [showSubMenueItemTwo, setShowSubMenueItemTwo] = useState(false);
    const [showSubMenueItemThree, setShowSubMenueItemThree] = useState(false);
    const [showSubMenueItemFour, setShowSubMenueItemFour] = useState(false);
    const [showSubMenueItemFive, setShowSubMenueItemFive] = useState(false);
    const [showSubMenueItemSix, setShowSubMenueItemSix] = useState(false);


    const showSubMenueOne = () => {
        setShownOne(!shownOne)
    }
    const showSubMenueTwo = () => {
        setShownTwo(!shownTwo)
    }
    const showSubMenueThree = () => {
        setShownThree(!shownThree)
    }
    const showSubMenueItemsOne = () => {
        setShowSubMenueItemOne(!showSubMenueItemOne);
    }
    const showSubMenueItemsTwo = () => {
        setShowSubMenueItemTwo(!showSubMenueItemTwo);
    }
    const showSubMenueItemsThree = () => {
        setShowSubMenueItemThree(!showSubMenueItemThree);
    }
    const showSubMenueItemsFour = () => {
        setShowSubMenueItemFour(!showSubMenueItemFour);
    }
    const showSubMenueItemsFive = () => {
        setShowSubMenueItemFive(!showSubMenueItemFive);
    }
    const showSubMenueItemsSix = () => {
        setShowSubMenueItemSix(!showSubMenueItemSix);
    }

    return (
        <div className={styles.container}>
            <nav className={styles.navbar}>
                <ul className={styles.manueList}>
                    <li className={styles.menueItems}>
                        <div className={styles.contain} onClick={showSubMenueItemsOne}>
                            <p>BEST SELLERS</p>
                            <div className={styles.UpDownArrow}>
                                <img src={arrow} alt='up-down-arrow' className={showSubMenueItemOne && styles.routateArrow}/>
                            </div>
                        </div>
                        <ul className={styles.subMenueList}>
                            <li className={showSubMenueItemOne ? styles.subMenueFirstChild : styles.closeSubeMenue}>
                                <div>
                                <span>BY CATEGORY</span>
                                {shownOne ?
                                    <button className={styles.plusButton} onClick={showSubMenueOne}>-</button> :
                                    <button className={styles.plusButton} onClick={showSubMenueOne}>+</button>
                                }
                                </div>
                                <ul className={shownOne ? styles.subMenueItems : styles.closeSubeMenue}>
                                    <li>Body & Hair Care</li>
                                    <li>Skincare</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li className={styles.menueItems}>
                        <div className={styles.contain} onClick={showSubMenueItemsTwo}>
                            <p>SKINCARE</p>
                            <div className={styles.UpDownArrow}>
                                <img src={arrow} alt='up-down-arrow' className={showSubMenueItemTwo && styles.routateArrow}/>
                            </div>
                        </div>
                        <ul className={styles.subMenueList}>
                            <li className={showSubMenueItemTwo ? styles.subMenueFirstChild : styles.closeSubeMenue}>
                                <div>
                                <span>BY CATEGORY</span>
                                {shownOne ?
                                    <button className={styles.plusButton} onClick={showSubMenueOne}>-</button> :
                                    <button className={styles.plusButton} onClick={showSubMenueOne}>+</button>
                                }
                                </div>
                                <ul className={shownOne ? styles.subMenueItems : styles.closeSubeMenue}>
                                    <li>Bestsellers</li>
                                    <li>Mini Bliss</li>
                                    <li>Cleansers</li>
                                    <li>Toner</li>
                                    <li>Exfoliators & Treatments</li>
                                    <li>Face Masks</li>
                                    <li>Moisturizers + SPF</li>
                                    <li>Serums</li>
                                    <li>Eye & Lip</li>
                                    <li>Sale</li>
                                    <li>Shop All Skincare</li>
                                </ul>
                            </li>
                        </ul>
                        <ul className={styles.subMenueList}>
                            <li className={showSubMenueItemTwo ? styles.subMenueFirstChild : styles.closeSubeMenue}>
                                <div>
                                <span>BY CONCERN</span>
                                {shownTwo ?
                                    <button className={styles.plusButton} onClick={showSubMenueTwo}>-</button> :
                                    <button className={styles.plusButton} onClick={showSubMenueTwo}>+</button>
                                }
                                </div>
                                <ul className={shownTwo ? styles.subMenueItems : styles.closeSubeMenue}>
                                    <li>Acne & Blemishes</li>
                                    <li>Discoloration</li>
                                    <li>Fine Lines</li>
                                    <li>Dullness</li>
                                    <li>Texture / pores</li>
                                    <li>Sensitive Skin</li>
                                    <li>Dryness</li>
                                </ul>
                            </li>
                        </ul>
                        <ul className={styles.subMenueList}>
                            <li className={showSubMenueItemTwo ? styles.subMenueFirstChild : styles.closeSubeMenue}>
                                <div>
                                    <span>SHOP BY INGREDIENT</span>
                                    {shownThree ?
                                        <button className={styles.plusButton} onClick={showSubMenueThree}>-</button> :
                                        <button className={styles.plusButton} onClick={showSubMenueThree}>+</button>
                                    }
                                </div>
                                <ul className={shownThree ? styles.subMenueItems : styles.closeSubeMenue}>
                                    <li>Glycolid Acid</li>
                                    <li>Hyaluronic Acid</li>
                                    <li>Niacinamide</li>
                                    <li>Retinol</li>
                                    <li>Salicylic Acid</li>
                                    <li>Vitamin C</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li className={styles.menueItems}>
                        <div className={styles.contain} onClick={showSubMenueItemsThree}>
                            <p>BODY & HAIR CARE</p>
                            <div className={styles.UpDownArrow}>
                                <img src={arrow} alt='up-down-arrow' className={showSubMenueItemThree && styles.routateArrow}/>
                            </div>
                        </div>
                        <ul className={styles.subMenueList}>
                            <li className={showSubMenueItemThree ? styles.subMenueFirstChild : styles.closeSubeMenue}>
                                <div>
                                    <span>BY CATEGORY</span>
                                    {shownOne ?
                                        <button className={styles.plusButton} onClick={showSubMenueOne}>-</button> :
                                        <button className={styles.plusButton} onClick={showSubMenueOne}>+</button>
                                    }
                                </div>
                                <ul className={shownOne ? styles.subMenueItems : styles.closeSubeMenue}>
                                    <li>Bestsellers</li>
                                    <li>Body Wash & Scrubs</li>
                                    <li>Body Moisturizers</li>
                                    <li>Hair Removal</li>
                                    <li>Body Firming</li>
                                    <li>Sale</li>
                                    <li>Shop all Body and Hair Care</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li className={styles.menueItems}>
                        <div className={styles.contain} onClick={showSubMenueItemsFour}>
                            <p>SETS & KITS</p>
                            <div className={styles.UpDownArrow}>
                                <img src={arrow} alt='up-down-arrow' className={showSubMenueItemFour && styles.routateArrow}/>
                            </div>
                        </div>
                        <ul className={styles.subMenueList}>
                            <li  className={showSubMenueItemFour ? styles.subMenueFirstChild : styles.closeSubeMenue}>
                                <div>
                                    <span>BY CATEGORY</span>
                                    {shownOne ?
                                        <button className={styles.plusButton} onClick={showSubMenueOne}>-</button> :
                                        <button className={styles.plusButton} onClick={showSubMenueOne}>+</button>
                                    }
                                </div>
                                <ul className={shownOne ? styles.subMenueItems : styles.closeSubeMenue}>
                                    <li>Body & Hair sets</li>
                                    <li>Skincare sets</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li className={styles.ShopAllMenueItems}>
                        <div className={styles.contain} onClick={showSubMenueItemsFive}>
                            <p>SHOP ALL</p>
                            <div className={styles.UpDownArrow}>
                                <img src={arrow} alt='up-down-arrow' className={showSubMenueItemFive && styles.routateArrow}/>
                            </div>
                        </div>
                        <ul className={showSubMenueItemFive ? styles.ShopAllSubMenueList : styles.closeSubeMenue}>
                            <li>BLISS ORIGINALS</li>
                            <li>SUMMER MUST HAVES</li>
                            <li>NEW</li>
                            <li>VEGAN SKINCARE</li>
                            <li>BESTSELLERS</li>
                            <li>SALE</li>
                        </ul>
                    </li>
                    <li className={styles.menueItems}>
                        <div className={styles.contain} onClick={showSubMenueItemsSix}>
                            <p>THIS IS BLISS</p>
                            <div className={styles.UpDownArrow}>
                                <img src={arrow} alt='up-down-arrow' className={showSubMenueItemSix && styles.routateArrow}/>
                            </div>
                        </div>
                        <ul className={styles.subMenueList}>
                            <li className={showSubMenueItemSix ? styles.subMenueFirstChild : styles.closeSubeMenue}>
                                <div>
                                    <span>ABOUT BLISS</span>
                                    {shownOne ?
                                        <button className={styles.plusButton} onClick={showSubMenueOne}>-</button> :
                                        <button className={styles.plusButton} onClick={showSubMenueOne}>+</button>
                                    }
                                </div>
                                <ul className={shownOne ? styles.subMenueItems : styles.closeSubeMenue}>
                                    <li>About US</li>
                                    <li>Find Us</li>
                                    <li>Who We Are</li>
                                    <li>Join the Happy Skin</li>
                                    <li>How We Give Back</li>
                                    <li>Sustsinability</li>
                                    <li>2021 Impact Report</li>
                                </ul>
                            </li>
                        </ul>
                        <ul className={styles.subMenueList}>
                            <li className={showSubMenueItemSix ? styles.subMenueFirstChild : styles.closeSubeMenue}>
                                <div>
                                    <span>PRODUCT DETAILS</span>
                                    {shownTwo ?
                                        <button className={styles.plusButton} onClick={showSubMenueTwo}>-</button> :
                                        <button className={styles.plusButton} onClick={showSubMenueTwo}>+</button>
                                    }
                                </div>
                                <ul className={shownTwo ? styles.subMenueItems : styles.closeSubeMenue}>
                                    <li>Ingredients Glossary</li>
                                </ul>
                            </li>
                        </ul>
                        <ul className={styles.subMenueList}>
                            <li className={showSubMenueItemSix ? styles.subMenueFirstChild : styles.closeSubeMenue}>
                                <div>
                                    <span>TAKE A QUIZE</span>
                                    {shownThree ?
                                        <button className={styles.plusButton} onClick={showSubMenueThree}>-</button> :
                                        <button className={styles.plusButton} onClick={showSubMenueThree}>+</button>
                                    }
                                </div>
                                <ul className={shownThree ? styles.subMenueItems : styles.closeSubeMenue}>
                                    <li>Clear skin 101</li>
                                    <li>Ingredients quiz</li>
                                    <li>Find my routine</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    );
};

export default HamMenue;