import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';

//componnets
import Banner from './Banner';
import Navbar from './Navbar';
import HamMenueIcon from './HamMenueIcon';
import SearchingProducts from '../SearchingProducts';
import HamMenue from './HamMenue';
//css
import styles from './scss/Header.module.css';

//images
import map from '../../assets/map.jpg';
import searchIcon from '../../assets/searching.jpg';
import login from '../../assets/login.jfif';
import bag from '../../assets/bag.png';
import close from '../../assets/close.png';

//context
import { productContext } from '../../context/SecondProductContextProvider';
import { cartContext } from '../../context/CartContextProvider';

const Header = () => {

    const { state } = useContext(cartContext)

    const product = useContext(productContext)

    const [searching,setSearching] = useState("");
    const [shown,setShown] = useState(false);
    const [showMenue, setShowMenue] = useState(false);
    
    const searchHandler = event => {
        setSearching(event.target.value);
    }
    const showSearchBox = () => {
        setShown(!shown)
    }
    const closeSearchBox = () => {
        setShown(!shown)
    }
    const showHamMenue = () => {
        setShowMenue(!showMenue)
    }

    const searchedProducts = product.filter(products => products.category.toLowerCase().includes(searching.toLowerCase()));

    return (
        <>
            <Banner/>
            <div style={{position:"sticky",top:"0",zIndex: "25"}}>
                <div className={styles.container}>
                    <div className={styles.headerContainer}>
                        <div className={styles.left_section}>
                            <div className={styles.hamMenueAIconContainer} onClick={showHamMenue}>
                                <HamMenueIcon/>
                            </div>
                            <div className={styles.left_sectionIitems}>
                                <div  onClick={showSearchBox} style={{cursor:"pointer"}}>
                                    <img src={searchIcon} alt='searchIcon'/>
                                    <p style={{color:"white"}}>search</p>
                                </div>
                            </div>
                            <div className={styles.left_sectionIitems}>
                                <Link to='yahoo.com'>
                                    <img src={map} alt='mapIcon'/>
                                    <p>find us</p>
                                </Link>
                            </div>
                        </div>
                        <h1 className={styles.header}>bliss</h1>
                        <div className={styles.right_section}>
                            <div className={styles.right_sectionIitems}>
                                <Link to='/login'>
                                    <img src={login} alt='loginIcon'/>
                                    <p>login</p>
                                </Link>
                            </div>
                            <div className={styles.right_sectionIitems}>
                                <Link to='/bag'>
                                    <span className={styles.counter}>{state.itemCounter}</span>
                                    <img src={bag} alt='bagIcon'/>
                                    <p>bag</p>
                                </Link>
                            </div>
                        </div>  
                    </div>
                    <Navbar/>
                </div>
                <div className={showMenue ? styles.HamMenueContainer : styles.closeHamMenue}>
                    <HamMenue/>
                </div>
                <div className={shown ? styles.active : styles.inActive}>
                    <img className={styles.closeIcon} src={close} alt='closeIcon' onClick={closeSearchBox}/>
                    <input type='text' onChange={searchHandler} value={searching} placeholder="search the store"/>
                    {searching.length > 0 &&
                        <div>
                            {searchedProducts.map(product => <SearchingProducts key={product.id}
                                                                    productData={product}
                                                                    />)}
                        </div> 
                    } 
                </div>
            </div>
        </>
    );
};

export default Header;