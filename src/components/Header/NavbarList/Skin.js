import React from 'react';
import { Link } from 'react-router-dom';

//css
import styles from '../scss/Skincare.module.css';



const Skin = () => {


    return (
        <>
            <div className={styles.container}>
                <div className={styles.skincare_submenue_list}>
                    <ul>
                        <li>BY CATEGORY</li>
                        <li><Link to='google.com'>Bestsellers</Link></li>
                        <li><Link to='google.com'>Mini Bliss</Link></li>
                        <li><Link to='google.com'>Cleansers</Link></li>
                        <li><Link to='google.com'>Toner</Link></li>
                        <li><Link to='google.com'>Exfoliators & Treatments</Link></li>
                        <li><Link to='google.com'>Face Masks</Link></li>
                        <li><Link to='google.com'>Moisturizers + SPF</Link></li>
                        <li><Link to='google.com'>Serums</Link></li>
                        <li><Link to='google.com'>Eye & Lip</Link></li>
                        <li><Link to='google.com'>Sale</Link></li>
                        <li><Link to='google.com'>Shop All Skincare</Link></li>
                    </ul>
                </div>
                <div className={styles.skincare_submenue_list}>
                    <ul>
                        <li>BY CATEGORY</li>
                        <li><Link to='google.com'>Acne & Blemishes</Link></li>
                        <li><Link to='google.com'>Discoloration</Link></li>
                        <li><Link to='google.com'>Fine Lines</Link></li>
                        <li><Link to='google.com'>Dullness</Link></li>
                        <li><Link to='google.com'>Texture / pores</Link></li>
                        <li><Link to='google.com'>Sensitive Skin</Link></li>
                        <li><Link to='google.com'>Dryness</Link></li>
                        
                    </ul>
                </div>
                <div className={styles.skincare_submenue_list}>
                    <ul>
                    <li>SHOP BY INGREDIENT</li>
                        <li><Link to='google.com'>Glycolid Acid</Link></li>
                        <li><Link to='google.com'>Hyaluronic Acid</Link></li>
                        <li><Link to='google.com'>Niacinamide</Link></li>
                        <li><Link to='google.com'>Retinol</Link></li>
                        <li><Link to='google.com'>Salicylic Acid</Link></li>
                        <li><Link to='google.com'>Vitamin C</Link></li>
                        
                    </ul>
                </div>
            </div>
            
        </>
    );
};

export default Skin;