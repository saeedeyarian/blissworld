import React from 'react';
import { Link } from 'react-router-dom';

//css
import styles from '../scss/BodyHaircare.module.css';

const BodyHaircare = () => {
    return (
        <>
            <div className={styles.container}>
                <ul>
                    <li>BY CATEGORY</li>
                    <li><Link to='google.com'>Bestsellers</Link></li>
                    <li><Link to='google.com'>Body Wash & Scrubs</Link></li>
                    <li><Link to='google.com'>Body Moisturizers</Link></li>
                    <li><Link to='google.com'>Hair Removal</Link></li>
                    <li><Link to='google.com'>Body Firming</Link></li>
                    <li><Link to='google.com'>Sale</Link></li>
                    <li><Link to='google.com'>Shop all Body and Hair Care</Link></li>
                </ul>
            </div>  
        </>
    );
};

export default BodyHaircare;