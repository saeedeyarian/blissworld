import React from 'react';
import { Link } from 'react-router-dom';
//css
import styles from '../scss/Navbar.module.css'

const BestSeller = () => {
    return (
        <div className={styles.bestSeller_subMenue}>
            <ul className={styles.subMenue_list}>
                <li className={styles.subMenue_items}>BY CATEGORY</li>
                <li className={styles.subMenue_items}><Link to='google.com'>Body & Hair Care</Link></li>
                <li className={styles.subMenue_items}><Link to='google.com'>Skincare</Link></li> 
            </ul>
        </div>
    );
};

export default BestSeller;