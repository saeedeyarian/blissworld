import React from 'react';
import { Link } from 'react-router-dom';

//css
import styles from '../scss/Bliss.module.css';

const Bliss = () => {
    return (
        <>
            <div className={styles.container}>
                <ul>
                    <li>ABOUT BLISS</li>
                    <li><Link to='google.com'>About US</Link></li>
                    <li><Link to='google.com'>Find Us</Link></li>
                    <li><Link to='google.com'>Who We Are</Link></li>
                    <li><Link to='google.com'>Join the Happy Skin</Link></li>
                    <li><Link to='google.com'>How We Give Back</Link></li>
                    <li><Link to='google.com'>Sustsinability</Link></li>
                    <li><Link to='google.com'>2021 Impact Report</Link></li>
                </ul>
                <ul>
                    <li>PRODUCT DETAILS</li>
                    <li><Link to='google.com'>Ingredients Glossary</Link></li>
                </ul>
            </div>  
        </>
    );
};

export default Bliss;