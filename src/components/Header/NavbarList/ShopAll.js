import React from 'react';
import { Link } from 'react-router-dom';

//css
import styles from '../scss/ShopAll.module.css';
const ShopAll = () => {
    return (
        <>
            <div className={styles.container}>
                <ul>
                    <li><Link to='google.com'>BLISS ORIGINALS</Link></li>
                    <li><Link to='google.com'>SUMMER MUST HAVES</Link></li>
                    <li><Link to='google.com'>NEW</Link></li>
                    <li><Link to='google.com'>VEGAN SKINCARE</Link></li>
                    <li><Link to='google.com'>BESTSELLERS</Link></li>
                    <li><Link to='google.com'>SALE</Link></li>
                </ul>
            </div>  
        </>
    );
};

export default ShopAll;