import React from 'react';
import { Link } from 'react-router-dom';

//components
import BestSeller from './NavbarList/BestSeller';
import Skin from './NavbarList/Skin';
import BodyHaircare from './NavbarList/BodyHaircare';
import ShopAll from './NavbarList/ShopAll';
import Bliss from './NavbarList/Bliss';

//css
import styles from './scss/Navbar.module.css';



const Navbar = () => {
    return (
        <div className={styles.container}>
            <nav className={styles.navMenue}>
                <ul className={styles.navMenue_list}>
                    <li className={styles.navMenue_items}>
                        <Link to="google.com">BEST SELLERS</Link>
                        <BestSeller/>
                    </li>
                    <li className={styles.navMenue_items}>
                        <Link to="Google.com">SKINCARE</Link>
                        <div className={styles.skincare_subMenue}>
                            <Skin/>
                        </div>
                    </li>
                    <li className={styles.navMenue_items}>
                        <Link to="google.com">BODY & HAIR CARE</Link>
                        <div className={styles.bodyHairecare_subMenue}>
                            <BodyHaircare/>
                        </div>
                    </li>
                    <li className={styles.navMenue_items}>
                        <Link to="google.com">SHOP ALL</Link>
                        <div className={styles.ShopAll_subMenue}>
                            <ShopAll/>
                        </div>
                    </li>
                    <li className={styles.navMenue_items}>
                        <Link to="google.com">THIS IS BLISS</Link>
                        <div className={styles.Bliss_subMenue}>
                            <Bliss/>
                        </div>
                    </li>
                </ul> 
            </nav>
        </div>
    );
};

export default Navbar;