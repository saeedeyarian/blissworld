import React, { createContext, useEffect, useState } from 'react';

import { getProdeuctSecondAPI } from '../services/Api';

export const productContext = createContext();

const SecondProductContextProvider = ({children}) => {

    const [product, setProducts] = useState([])

    useEffect( () => {
        const fetchApi = async() => {
            setProducts(await getProdeuctSecondAPI())
        }

        fetchApi();
    },[])

    return (
        <productContext.Provider value={product}>
            {children}
        </productContext.Provider>
    );
};

export default SecondProductContextProvider;