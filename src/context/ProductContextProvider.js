import React, {  createContext, useEffect, useState } from 'react';
//api
import { getProduct } from '../services/Api';

export const productContext = createContext()

const ProductContextProvider = ({children}) => {

    const [product ,setProduct] = useState([])

    useEffect ( () => {
        const fetchApi = async() => {
            setProduct (await getProduct())
        }
        fetchApi()
    },[])

    return (
        <productContext.Provider value={product}>
            {children}
        </productContext.Provider>
    );
};

export default ProductContextProvider;