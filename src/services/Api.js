import axios from "axios";
const BASE_URL = "https://api.escuelajs.co/api/v1/categories";
const Second_BASE_URL = "https://fakestoreapi.com/products";


const getProduct = async() => {
    const response = await axios.get(`${BASE_URL}`)
    return response.data;
}

const getProdeuctSecondAPI = async () => {
    const responses = await axios.get(`${Second_BASE_URL}`)
    return responses.data;
}

export {getProduct, getProdeuctSecondAPI};