import './App.css';
import {Routes, Route} from 'react-router-dom';

//context
import ProductContextProvider from './context/ProductContextProvider';
import SecondProductContextProvider from './context/SecondProductContextProvider';
import CartContextProvider from './context/CartContextProvider';
//components
import Body from './components/Body';
import Header from './components/header/Header';
import Footer from './components/Footer';
import Login from './components/logIn/Login';
import Bag from './components/ShoppingBag'
import Details from './components/Detailes';

function App() {
  return (
    <CartContextProvider>
    <SecondProductContextProvider>
    <ProductContextProvider>
      <Header/>
      <Routes>
        <Route path='/' element={<Body/>}/>
        <Route path='/login' element={<Login/>}/>
        <Route path='/Bag' element={<Bag />}/>
        <Route path='/:id' element={<Details/>}/>
      </Routes>
      <Footer/>
    </ProductContextProvider>
    </SecondProductContextProvider>
    </CartContextProvider>
  );
}

export default App;
